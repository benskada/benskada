x12=read.csv('after12months.dat')
young12 = factor(x12$young)
dead12 = factor(x12$dead)
late12 = factor(x12$late)
early12 = factor(x12$early)
kon12 = factor(x12$kon)
age12 = factor(x12$age)
severe12= factor(x12$severe)
amp12 = factor(x12$amp)
lambo12 = factor(x12$lambo)
week12 = factor(x12$week)
A12 =factor(x12$A)
B12 =factor(x12$B)
C12 =factor(x12$C)
Y12 =factor(x12$Y)
A12B12 = factor(x12$A*x12$B)
A12C12 = factor(x12$A*x12$C)
B12C12 = factor(x12$B*x12$C)
A12B12C12 = factor(x12$A*x12$B*x12$C)

xx12 <- data.frame(x12$id,young12,late12,early12,dead12,kon12,age12,severe12,amp12,lambo12,week12,A12,B12,C12,A12B12,A12C12,B12C12,A12B12C12,Y12)

yy12  = xx12[which(xx12$young12==0),]
zz12  = yy12[which(yy12$week12==1),]
ww12  = xx12[which(xx12$A12==1 |  xx12$B12==1 |  xx12$C12==1),]
xxx12 = xx12[which(xx12$young12==0 & xx12$dead12==0 & xx12$late12 == 0 & xx12$early12 == 0),]

ww12  = yy12[which(yy12$A12==1),]

print(summary(xx12))
print(summary(yy12))
print(summary(zz12))
print(summary(ww12))
print(summary(xxx12))
print('ww')
print(summary(ww12))

g12=glm(Y12 ~ kon12 + age12 + severe12 + A12 + B12*C12,data=xxx12,family = binomial)

print(exp(cbind("Odds ratio" = coef(g12), confint.default(g12, level = 0.95))))
print(summary(g12))

xxxx12=xxx12[which(xxx12$Y12==1),]
print(summary(xxxx12))