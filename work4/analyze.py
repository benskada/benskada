import csv
from datetime import date

x = date(1990,1,1)
def parse_date(d):
    if '-' in d:
        year  = int(d[0:4])
        month = int(d[5:7])
        day   = int(d[8:10])
    else:
        year  = int(d[0:4])
        month = int(d[4:6])
        day   = int(d[6:8])

    if month == 0: month=1
    if day   == 0: day = 1
    return (date(year,month,day)-x).days

# We shall find all prescriptions within 30 days before first day in hospital
within_days = 3*30

# then examinatin date is after this many days
examine_days = 12*30
prescription_days = 30

#end date of study
end_date    = parse_date('201901231')
start_date  = parse_date('200101231')

opioids = ['N02AA','N02AB','N02AC','N02AJ','N02AE','N02AX']
benz    = ['N05BA']
antiDep = ['N06AA','N06AB','N06AX']

def parse_medic(l):
    def isX(atc,L):
        for x in L:
            if atc.startswith(x):
                return 1
        return 0

    def isA(atc):
        return isX(atc,opioids)
    def isB(atc):
        return isX(atc,benz)
    def isC(atc):
        return isX(atc,antiDep)
    
    
    A = 0
    B = 0
    C = 0
    for atc in l:
        atc = atc.upper()
        if isA(atc):
            A=1
        if isB(atc):
            B=1
        if isC(atc):
            C=1

    return A,B,C

opcodes_lambo = ['ZZQ','ZZS','ZZA00']
opcodes_amp   = ['NFQ19','NGQ09','NGQ19']
opcodes_severe = opcodes_lambo + opcodes_amp

def parse_severe(l):
    sev = 0
    for x in l:
        x = x.upper()
        if not x: continue

        for op in opcodes_severe:
            if x.startswith(op):
                sev=1
    return sev

def parse_lambo(l):
    sev = 0
    for x in l:
        x = x.upper()
        if not x: continue

        for op in opcodes_lambo:
            if x.startswith(op):
                sev=1
    return sev

def parse_amp(l):
    sev = 0
    for x in l:
        x = x.upper()
        if not x: continue

        for op in opcodes_amp:
            if x.startswith(op):
                sev=1
    return sev

def parse_ålder(age):
    if age < 15:
        return 0
    elif age <= 40:
        return 1
    elif age <= 65:
        return 3
    else:
        return 4
    

death = dict()

with open('R_DORS__31876_2020.txt','r',encoding='latin-1') as fd:
    r = csv.reader(fd,delimiter='\t')
    header = False
    for l in r:
        if not header:
            header=l
            continue
    
        death[int(l[0])] = parse_date(l[1])


medic = dict()
with open('R_LMED__31876_2020.txt','r',encoding='latin-1') as fd:
    r = csv.reader(fd,delimiter='\t')

    header = False    
    for l in r:
        if not header:
            header=l
            continue
        
        nr     = int(l[0])
        atc    = l[1]
        edatum = parse_date(l[4])
        fdatum = parse_date(l[5])

        v = medic.get(nr,[])
        v.append((edatum,atc,l))
        medic[nr]=v

sick = dict()

with open('T_T_R_PAR_SV_31876_2020.txt','r') as fd:
    r = csv.reader(fd,delimiter='\t')

    header = False    
    for l in r:
        if not header:
            header=l

            opi = header.index('op1')
            continue

        nr      = int(l[2])
        datum1  = parse_date(l[9])
        datum2  = parse_date(l[1])
        datum   = min(datum1,datum2)
        kön     = int(l[5])
            
        if l[6]:
            ålder   = int(l[6])
        else:
            ålder   = 0
            
        ecodes  = l[16:16+5]
        opcodes = l[opi:opi+30]

        severe  = parse_severe(opcodes)
        lambo   = parse_lambo(opcodes)
        amp     = parse_amp(opcodes)

        v = sick.get(nr,[])
        v.append((datum,kön,ålder,severe,lambo,amp,l))
        sick[nr]=v

table = list()
s=0
find=set()
opios=list()
presc=list()
for k,l in sick.items():
    l.sort()

    sev = 0
    s_amp = 0
    s_lambo = 0
    for d,kön,ålder,severe,lambo,amp,u in l:
        if severe: sev = 1
        if lambo : s_lambo = 1
        if amp   : s_amp   = 1

    date,kön,ålder,*x = l[0]

    agegroup = parse_ålder(ålder)

    s15 = 0
    # agegroup 0 will not be included
    if not agegroup:
        s15 = 1
    
    dd = date + examine_days

    # we only include persons stil in the study at examnie date
    slate = 0
    if dd > end_date:
        slate = 1

    ddd = date - prescription_days
    searly = 0
    if ddd < start_date:
        searly = 1

    
    dead = death.get(k,0)
    if dead:
        if dead < dd:
            dead = 1
        else:
            dead = 0

    l = medic.get(k,[])
    l.sort()
    
    ll = []
    
    for d,atc,u in l:        
        if d >= date - within_days and d < date:
            ll.append(atc)
    A1,B1,C1 = parse_medic(ll)

    ll = []
    for d,atc,u in l:        
        if d >= date - 7 and d <= date:
            ll.append(atc)
    A3,B3,C3 = parse_medic(ll)

    v1 = 0 
    if A3+B3+C3 > 0:
        v1 = 1
        presc.append((k,(A3,B3,C3)))
    
    if A1:
        find.add(k)
        
    lll = []
    for d,atc,u in l:        
        if d >= dd - within_days and d <= dd:
            lll.append(atc)

    A2,B2,C2 = parse_medic(lll)

    if A2==1 and not dead and not slate and not s15 and not searly:
        opios.append(k)
    Y=A2

    table.append((k,dead,s15,slate,searly,kön,agegroup,sev,s_amp,s_lambo,
                  v1,A1,B1,C1,Y))


presc.sort()
opios.sort()
#for k in opios:
#    for d,kön,ålder,severe,lambo,amp,u in sick[k]:
#        print(k,*u)

#    for d,atc,u in medic.get(k,[]):
#        print(k,*u)
#exit(0)
#print('nr:',len(presc))
#print('lpnr (opioid benz antidep)')
#for x in presc:
#    print(x)
#exit(0)
    
table.sort()
print('id,dead,young,late,early,kon,age,severe,amp,lambo,week,A,B,C,Y')
for x in table:
    s = str(x[0])
    for xx in x[1:]:
        s += ','+str(xx)
    print(s)

