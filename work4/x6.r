x6=read.csv('after6months.dat')
young6 = factor(x6$young)
dead6 = factor(x6$dead)
late6 = factor(x6$late)
early6 = factor(x6$early)
kon6 = factor(x6$kon)
age6 = factor(x6$age)
severe6= factor(x6$severe)
amp6 = factor(x6$amp)
lambo6 = factor(x6$lambo)
A6 =factor(x6$A)
B6 =factor(x6$B)
C6 =factor(x6$C)
Y6 =factor(x6$Y)

xx6 <- data.frame(young6,late6,early6,dead6,kon6,age6,severe6,amp6,lambo6,A6,B6,C6,Y6)

yy6  = xx6[which(xx6$young24==0),]

xxx6 = xx6[which(xx6$young6==0 & xx6$dead6==0 & xx6$late6 == 0 & xx6$early6 == 0),]
print(summary(xx6))
print(summary(yy6))
print(summary(xxx6))

g6=glm(Y6 ~ kon6 + age6 + severe6 + A6 + B6*C6,data=xxx6,family = binomial)

print(exp(cbind("Odds ratio" = coef(g6), confint.default(g6, level = 0.95))))
print(summary(g6))

xxxx6=xxx6[which(xxx6$Y6==1),]
print(summary(xxxx6))