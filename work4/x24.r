x24=read.csv('after24months.dat')
young24 = factor(x24$young)
dead24 = factor(x24$dead)
late24 = factor(x24$late)
early24 = factor(x24$early)
kon24 = factor(x24$kon)
age24 = factor(x24$age)
severe24 = factor(x24$severe)
amp24 = factor(x24$amp)
lambo24 = factor(x24$lambo)
A24 =factor(x24$A)
B24 =factor(x24$B)
C24 =factor(x24$C)
Y24 =factor(x24$Y)

xx24 <- data.frame(young24,late24,early24,dead24,kon24,age24,severe24,amp24,lambo24,A24,B24,C24,Y24)

yy24  = xx24[which(xx24$young24==0),]

xxx24 = xx24[which(xx24$young24==0 & xx24$dead24==0 & xx24$late24 == 0 & xx24$early24 == 0),]

print(summary(xx24))
print(summary(yy24))
print(summary(xxx24))

g24=glm(Y24 ~ kon24 + age24 + severe24 + A24 + B24*C24,data=xxx24,family = binomial)

print(exp(cbind("Odds ratio" = coef(g24), confint.default(g24, level = 0.95))))
print(summary(g24))

xxxx24=xxx24[which(xxx24$Y24==1),]
print(summary(xxxx24))
