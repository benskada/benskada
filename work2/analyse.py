import csv
fd = open('INDEX_PAR_SV_28622_2017.txt','r')
r = csv.reader(fd,delimiter='\t')

l = list(r)
fd.close()

alla = set()
for dl in l[1:]:
    alla.add(int(dl[0]))

opcodes_lambo = ['ZZQ','ZZS','ZZA00']
opcodes_amp   = ['NFQ19','NGQ09','NGQ19']
opcodes_severe = opcodes_lambo + opcodes_amp

severe = set()
for dl in l[1:]:
    ops = dl[20].split()
    for op in ops:
        for lam in opcodes_severe:
            if op.startswith(lam):
                severe.add(int(dl[0]))
        
mild = alla - severe

import random
def rand():
    while True:
        yield random.uniform(0,1)

lmild   = list(zip(rand(),mild))
lsevere = list(zip(rand(),severe))

lmild.sort()
lsevere.sort()

subset_mild = []
for i,dl in zip(range(150),lmild):
    subset_mild.append(dl[1])
subset_mild.sort()
    
subset_severe = []
for i,dl in zip(range(150),lsevere):
    subset_severe.append(dl[1])

subset_severe.sort()

f1 = open('severe_id.dat','w')
f2 = open('mild_id.dat','w')

fs1 = open('severe.dat','w')
fs2 = open('mild.dat','w')

r1  = csv.writer(f1,delimiter='\t')
r2  = csv.writer(f2,delimiter='\t')
rs1 = csv.writer(fs1,delimiter='\t')
rs2 = csv.writer(fs2,delimiter='\t')

for i in subset_severe:
    r1.writerow([i])
for i in subset_mild:
    r2.writerow([i])

smild = set(subset_mild)
ssevere = set(subset_severe)

for dl in l[1:]:
    if int(dl[0]) in smild:
        rs2.writerow(dl)
    if int(dl[0]) in ssevere:
        rs1.writerow(dl)

f1.close()
f2.close()
fs1.close()
fs2.close()
