mild   = read.csv("mild.csv",header=TRUE)
mildk  = mild[which(mild$Kön == 2),]
mildm  = mild[which(mild$Kön == 1),]
mild   = mild[which(mild$Resultat1==1 | mild$Resultat1==2),]
mildkvinnor = mild[which(mild$Kön == 2),]
mildmän = mild[which(mild$Kön == 1),]

xmk = hist(mildk$Ålder)
xmm = hist(mildm$Ålder)


severe = read.csv("severe.csv",header=TRUE)
severek = severe[which(severe$Kön == 2),]
severem = severe[which(severe$Kön == 1),]

severe = severe[which(severe$Resultat1==1 | severe$Resultat1==2),]
severekvinnor = severe[which(severe$Kön == 2),]
severemän = severe[which(severe$Kön == 1),]

xsk = hist(severek$Ålder)
xsm = hist(severem$Ålder)


#Ålder
print(t.test(mild$Ålder,severe$Ålder,paired=FALSE, alternative="two.sided"))
print('mild mean sd')
print(sd(mild$Ålder))
print(mean(mild$Ålder))
print('severe mean sd')
print(sd(severe$Ålder))
print(mean(severe$Ålder))

print('severe kvinnor mean sd')
print(sd(severekvinnor$Ålder))
print(mean(severekvinnor$Ålder))

print('severe män mean sd')
print(sd(severemän$Ålder))
print(mean(severemän$Ålder))

print('mild kvinnor mean sd')
print(sd(mildkvinnor$Ålder))
print(mean(mildkvinnor$Ålder))

print('mild män mean sd')
print(sd(mildmän$Ålder))
print(mean(mildmän$Ålder))


# Kön 
print(t.test(mild$Kön,severe$Kön,paired=FALSE, alternative="two.sided"))

mild   = mild[which(mild$Resultat1==1),]
severe = severe[which(severe$Resultat1==1),]

# Count
mildc = read.csv("mild_count.dat",header=TRUE)
severec = read.csv("severe_count.dat",header=TRUE)
print(t.test(mildc$a,severec$a,paired=FALSE, alternative="two.sided"))

# GUstillo
mildTrue   = mild[which(mild$Klassificeringsmetod==1),]
severeTrue = severe[which(severe$Klassificeringsmetod==1),]

n_mildTrue  = length(mildTrue$Kön)
n_mildFalse = length(mild$Kön) - n_mildTrue

n_severeTrue  = length(severeTrue$Kön)
n_severeFalse = length(severe$Kön) - n_severeTrue

x = matrix(c(n_mildTrue,n_mildFalse,n_severeTrue,n_severeFalse),nrow=2)

print(fisher.test(x,NULL))

# Linje histogram
# TODO

#frequency
print(prop.test(76, 86, conf.level=0.95, correct = FALSE))
print(prop.test(74, 77, conf.level=0.95, correct = FALSE))
x = matrix(c(76,10,74,3),nrow=2)
print(fisher.test(x,NULL))


print(prop.test(116, 133, conf.level=0.95, correct = FALSE))
print(prop.test(121, 126, conf.level=0.95, correct = FALSE))
print(prop.test(237, 259, conf.level=0.95, correct = FALSE))
xx = matrix(c(121,5,116,17),nrow=2)
print(fisher.test(xx,NULL))