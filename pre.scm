(use-modules (ice-9 rdelim))
(use-modules (ice-9 match))
(use-modules (srfi  srfi-19))
(use-modules (srfi  srfi-1))


;Amputationer
;------------
;NGQ19
;NFQ19
;NHQ14
;NGQ09
;NHQ09
;NHQ11
;NHQ13


(define (fall0 x)
  (match x
   ((#\W #\0 . _) #t)
   (_             #f)))

(define (fall1 x)
  (match x
   ((#\W #\1 . _) #t)
   (_             #f)))

(define (fall2 x)
  (match x
   ((#\W _ . _) #t)
   (_             #f)))

(define (olycka-09 x)
  (match x
   ((#\V #\0 #\9 . _) #t)
   (_                 #f)))

(define (olycka-1 x)
  (match x
   ((#\V #\1 . _) #t)
   (_             #f)))

(define (olycka-2 x)
  (match x
   ((#\V #\2 . _) #t)
   (_             #f)))

(define (olycka-4 x)
  (match x
   ((#\V #\4 . _) #t)
   (_             #f)))

(define (olycka-6 x)
  (match x
   ((#\V #\6 . _) #t)
   (_             #f)))

(define (olycka-7 x)
  (match x
   ((#\V #\7 . _) #t)
   (_             #f)))

(define (olycka-8 x)
  (match x
   ((#\V #\8 . _) #t)
   (_             #f)))

(define (gift x)
  (match x
    ((#\X . _) #t)
    (_         #f)))

(define (get-accident-type-2 x)
  (define (take x)
    (list (car x)))
  (let ((E (take (map string->list (map symbol->string (get-E-codes x))))))
    (cond
     ((find fall0      E) 10)
     ((find fall1      E) 0)
     ((find fall2      E) 12)
     ((find olycka-09  E) 9)
     ((find olycka-1   E) 1)
     ((find olycka-2   E) 2)
     ((find olycka-4   E) 4)
     ((find olycka-8   E) 8)
     ((find gift       E) 20)
     ((null? E)           100)
     (else                200))))

(define (trafikolycka? X)
  (eq? (car X) #\V))

(define (fall-i-samma-plan X)
  (match X
    ((#\W #\0 (or #\0 #\1) . _) #t)
    ((#\W #\1 #\8          . _) #t)
    (_ #f)))

(define (fall-från-höjder X)
  (match X
    ((#\W #\1 (or #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7) . _)
     #t)
    (_ #f)))

(define (fall-ospecificerat X)
  (match X
    ((#\W #\1 #\9 . _) #t)
    (_ #f)))


(define (get-E-codes x)
  (define (get-ekod x)
    (filter
     (lambda (x) (not (equal? "" x)))
     (match (ncdr x 16)
       ((a b c d e f g h i . _)
        (list a b c d e f g h i)))))

  (let lp ((E '()) (x x))
    (match x
      ((x . u)
       (let lpp ((l (get-ekod x)) (E E))
         (if (pair? l)
             (if (memq (car l) E)
                 (lpp (cdr l) E)
                 (lpp (cdr l) (cons (car l) E)))
             (lp E u))))
      (() (reverse E)))))

;; 0 - not specified
;; 1 - other fall
;; 2 - same plane
;; 3 - hights
;; 4 - car accident

(define (get-accident-type x)
  (let ((E (map string->list (map symbol->string (get-E-codes x)))))
    (cond
     ((find trafikolycka?      E) 5)
     ((find fall-från-höjder   E) 4)
     ((find fall-i-samma-plan  E) 3)
     ((find fall-ospecificerat E) 2)
     ((null? E)                   0)
     (else                        1))))

(define-syntax-rule (aif (it) p x y) (let ((it p)) (if it x y)))

(define (ncdr x n)
  (if (= n 0)
      x
      (ncdr (cdr x) (- n 1))))

(define (get-op-d x) (car (ncdr x 27)))

(define (get-opd x)
  (let ((res (car (ncdr x 25))))
    (if (pair? res)
        res
        (list res))))


(define ndate 9)
(define (get-opdates x)
  (ncdr x 27))

(define (digitize x)
  (if x 1 0))


(define diagnoser '(S8211 S8221 S8231))
(define (find-diagnose m l)
  (define (mb x l)
    (if (pair? l)
        (member x l)
        (eq? x l)))

  (let ((hd (list-ref l 4))
        (ds (list-ref l 5)))
    (or (mb m  hd)
        (mb m ds))))

(define exfixcode 'NGJ29)
(define mspikcode 'NGJ59)
(define plattcode  'NGJ69)
(define metacodes  '(NGJ09 TNG33 TNG32))
(define restcode  (string->list "NGJ"))
(define nocode    (list 'NGJ19 exfixcode mspikcode plattcode))
(define (ngj-rest? in)
  (let lp ((x (string->list (symbol->string in)))
	   (y restcode))
    (if (pair? y)
	(if (equal? (car y) (car x))
	    (lp (cdr x) (cdr y))
	    #f)
	(not (member in nocode)))))


(define (bara bara else)
  (define (b x)
    (let ((codes (get-opd x)))
      (or-map (lambda (x) (member x bara)) codes)))
  (define (e x)
    (let ((codes (get-opd x)))
      (or-map (lambda (x) (member x else)) codes)))

  (lambda (me)
    (and (or-map b me)
	 (not (or-map e me)))))

(define (is bara)
  (define (b x)
    (let ((codes (get-opd x)))
      (or-map (lambda (x) (member x bara)) codes)))
  (lambda (me) (or-map b me)))


(define allop '(NGJ59 NGJ69 NGJ09 TNG32 TNG33 NGJ29 NGJ49 NGJ89 NGJ79 NGJ99))
(define (diff x)
  (let lp ((l allop))
    (if (pair? l)
	(if (member (car l) x)
	    (lp (cdr l))
	    (cons (car l) (lp (cdr l))))
	'())))

(define mrgspik-bara (bara '(NGJ59) '(NGJ29)))
(define platta-bara  (bara '(NGJ69) '(NGJ29)))
(define rep/gips     (bara '(NGJ09 TNG32 TNG33) (diff '(NGJ09 TNG32 TNG33))))
(define fid-slut     (bara '(NGJ29) (diff '(NGJ29))))

(define (the-5 me)
  (define (f a b)
    (let ((x (bara (append a b) (diff (append a b))))
	  (y (is a))
	  (z (is b)))
      (and (x me) (y me) (z me))))
  (or (f '(NGJ29) allop)
      (f '(NGJ29) '(NGJ49 NGJ89 NGJ79 NGJ99))))

(define (the-op-code me)
  (cond 
   ((mrgspik-bara me) 1)
   ((platta-bara  me) 2)
   ((rep/gips     me) 3)
   ((fid-slut     me) 4)
   ((the-5        me) 5)
   (else              6)))



      


(define (arest0? in)
  (member in metacodes))

(define (ngj? x)
  (let ((codes (get-opd x)))
    (or-map ngj-rest? codes)))

(define (ex? x)
  (let ((codes (get-opd x)))
    (or-map (lambda (x) (eq? x exfixcode)) codes)))

(define (ms? x)
  (let ((codes (get-opd x)))
    (or-map (lambda (x) (eq? x mspikcode)) codes)))

(define (pl? x)
  (let ((codes (get-opd x)))
    (or-map (lambda (x) (eq? x plattcode)) codes)))
  
(define (ex-ms? x)
  (let ((codes (get-opd x)))
    (and (or-map (lambda (x) (eq? x exfixcode)) codes)
	 (or-map (lambda (x) (eq? x mspikcode)) codes))))

(define (ex-pl? x)
  (let ((codes (get-opd x)))
    (and (or-map (lambda (x) (eq? x exfixcode)) codes)
	 (or-map (lambda (x) (eq? x plattcode)) codes))))

(define (ex-re? x)
  (let ((codes (get-opd x)))
    (and (or-map (lambda (x) (eq? x exfixcode)) codes)
	 (or-map ngj-rest? codes))))

(define (arest? in)
  (let ((codes (get-opd in)))
    (and (not (or-map (lambda (p?) (p? in))
		      (list ex? ms? pl?)))
	 (or-map arest0? codes))))



(define ampcodes '(NGQ19 NFQ19 NHQ14 NGQ09 NHQ09 NHQ11 NHQ13))
(define plastic-codes
  '(ZZR05 ZZR10 ZZR20 ZZR30 ZZR40 ZZS00 ZZS10 ZZS20 ZZS40 ZZM00 
          ZZQ00 ZZQ10 ZZQ20 ZZQ30 ZZQ40 ZZQ60 
          ZZR00 ZZA00))

(define plastic-codes2
  '(ZZR05 ZZR10 ZZR20 ZZR30 ZZR40 ZZS00 ZZS10 ZZS20 ZZS40 ZZM00 
          ZZQ00 ZZQ10 ZZQ20 ZZQ30 ZZQ40 ZZQ60 
          ZZR00))

(define plastic-int '(ZZQ00 ZZQ10 ZZQ20 ZZQ30 ZZQ40 ZZQ60))
(define plastic      (reverse (cdr (reverse plastic-codes))))
(define plastic-a00 '(ZZA00))
(define plastic-r   '(ZZR05 ZZR10 ZZR20 ZZR30 ZZR40 ZZR00 ZZS00 ZZS10 ZZS20 ZZS40 ZZM00))
(define plastic-q   '(ZZQ00 ZZQ10 ZZQ20 ZZQ30 ZZQ40 ZZQ60 ))

(define (amputition? xx)
  (let ((codes (get-opd xx)))
    (or-map (lambda (x) (and (member x ampcodes) (time-to-end (get-op-d xx))))
	    codes)))

(define (plastic-no-a00? x)
  (let ((codes (get-opd x)))
    (or-map (lambda (x) (member x plastic))
            codes)))

(define (plastic-a? x)
  (let ((codes (get-opd x)))
    (or-map (lambda (x) (member x plastic-a00))
            codes)))

(define (plastic-r? x)
  (let ((codes (get-opd x)))
    (or-map (lambda (x) (member x plastic-r))
            codes)))

(define (plastic-q? x)
  (let ((codes (get-opd x)))
    (or-map (lambda (x) (member x plastic-q))
            codes)))

(define (nplastic? x)
  (let lp ((codes (get-opd x)) (i 0))
    (if (pair? codes)
	(if (member (car codes) plastic-codes2)
	    i
	    (lp (cdr codes) (+ i 1)))
	#f)))

(define (plastic? x)
  (let ((codes (get-opd x)))
    (or-map (lambda (x) (member x plastic-codes))
            codes)))

(define (plastic-int? x)
  (let ((codes (get-opd x)))
    (or-map (lambda (x) (member x plastic-int))
            codes)))

(define (make-opdata x)
  (let loop ((ops ampcodes))
    (match ops
      ((op . ops)
       (if (or-map (lambda (x)
                     (member op (get-opd x)))
                   x)
           (cons 1 (loop ops))
           (cons 0 (loop ops))))
      (() '()))))

(define (make-plastic-opdata x)
  (let loop ((ops plastic-codes))
    (match ops
      ((op . ops)
       (if (or-map (lambda (x)
                     (member op (get-opd x)))
                   x)
           (cons 1 (loop ops))
           (cons 0 (loop ops))))
      (() '()))))
      


(define (first-date x)
  (list-ref (car x) ndate))

(define (month-first x)
  (date-month (first-date x)))

(define (amp-3 x n)
  (let ((dfirst (first-date x))
        (amp-date 
         (let loop ((x x))
           (match x
             ((x . l)
              (if (amputition? x)
                  (let loop2 ((codes (get-opd x)) (dt (get-opdates x)))
                    (match codes
                      ((c . u)
                       (if (member c ampcodes)
                           (let ((ret (car dt)))
                             (if (eq? ret 'empty)
                                 (list-ref x ndate)
                                 ret))
                           (loop2 (cdr codes) (cdr dt))))
                      (_ (loop l))))
                  (loop l)))
             (() #f)))))
    (if amp-date
        (let ((n-days (+  (* 1    (- (date-day amp-date)
                                     (date-day dfirst)))
                          (* 30   (- (date-month amp-date)
                                     (date-month dfirst)))
                          (* 365  (- (date-year amp-date)
                                     (date-year dfirst))))))
          (if (<= n-days n)
              1
              0))
        0)))

                      


(define stream  (open-file "par_lpnr_spss_new.csv" "r"))
(define stream2 (open-file "dor.csv" "r"))

(define (rd s)
  (call-with-input-string s
    (lambda (port) (read port))))

(define pr #f)

(define (analyze x)
  (match x
    ((month day year)
     (let ((r (make-date 0 0 0 0 (rd day) (rd month) (rd year) 0)))
       r))
    (("")   'empty)
    ((" ")  'empty)
    (("  ") 'empty)

    ((x)    (match (string-split x #\space)
              ((x) (rd x))
              ((and x (_ . _)) (map rd x))))

    ((x       (error (format #f "error with ~s" x))))))

(define header #f)

(define all-map (make-hash-table))
(define pat-map (make-hash-table))
(define (add x v)
  (let ((w (hash-ref pat-map x '())))
    (hash-set! pat-map x (cons v w))))

(define li (let loop ((l '()))             
             (let* ((r-d (read-line stream 'split))
                    (r   (car r-d)))
               (if (eof-object? r)
                   (reverse l)
                   (let* ((r   (list->string 
                               (reverse (cdr (reverse (string->list r))))))
                         (r    (map (lambda (x) (analyze (string-split x #\/)))
                                    (string-split r #\,)))
                         (d   (cdr r-d)))
                     (add (list-ref r 0) r-d)
                     (if (eof-object? d)
                         (reverse (cons r l))
                         (let ((age (list-ref r 2)))
                           (if (number? age)
                               (loop (cons r l))
                               (begin
                                 (set! header r)
                                 (loop l))))))))))

(pk 'length1 (length li))

(define dor (let loop ((l '()))             
             (let* ((r-d (read-line stream2 'split))
                    (r   (car r-d)))
               (if (eof-object? r)
                   (reverse l)
                   (let* ((r   r #;(list->string 
                               (reverse (cdr (reverse (string->list r))))))
                         (r    (map (lambda (x) (analyze (string-split x #\/)))
                                    (string-split r #\,)))
                         (d   (cdr r-d)))
                     (if (eof-object? d)
                         (reverse (cons r l)))
                         (loop (cons r l)))))))
           
(pk (length dor))
(pk 'part1-done)

(define li2 (let loop ((l li) (n (caar li)) (r '()) (rr '()))
              (if (pair? l)
                  (if (= (caar l) n)
                      (loop (cdr l) n (cons (car l) r) rr)
                      (loop (cdr l) (caar l) (cons (car l) '()) 
                            (cons (reverse r) rr)))
                  (reverse (cons (reverse r) rr)))))
(pk 'length2 (length li2))
(pk 'part2-done)

(define (opdate-ref l d)
  (let lp ((l (ncdr l 27)))
    (if (pair? l)
	(if (struct? (car l))
	    (let lp ((l l) (i d))
	      (if (pair? l)
		  (if (struct? (car l))
		      (if (= i 0)
			  (car l)
			  (lp (cdr l) (- i 1)))
		      (lp (cdr l) i))
		  -1))
	    (lp (cdr l)))
	-1)))

(define (time-to-end x)
  (let* ((dx (if (pair? x) (list-ref x 9) x)))
    (cond
     ((eq? dx 'empty)
      #f)
     (else
      (+ (* 1     (- 30   (date-day   dx)))
	 (* 30    (- 12   (date-month dx)))
	 (* 365   (- 2010 (date-year  dx))))))))

(define (time-to-base base x)
  (let* ((dx (if (pair? x) (list-ref x 9) x)))
    (cond
     ((eq? base 'empty)
      #f)
     ((struct? base)
      (+ (* 1     (- (date-day   base) (date-day   dx)))
	 (* 30    (- (date-month base) (date-month dx)))
	 (* 365   (- (date-year  base) (date-year  dx)))))
     (else
      #f))))

(define (xx->xxxx x) x)

(define (time-to-base2 dx base)
  (let* ((base (if (pair? base) (list-ref base 9) base)))
    (cond
     ((eq? dx 'empty)
      #f)
     ((struct? dx)
      (+ (* 1     (- (date-day   dx)            (date-day   base)))
	 (* 30    (- (date-month dx)            (date-month base)))
	 (* 365   (- (xx->xxxx (date-year  dx)) (date-year  base)))))
     (else
      #f))))
  


(define (less x y)
  (define (f d)
    (+ (* 1     (date-day   d))
       (* 100   (date-month d))
       (* 10000 (date-year  d))))
  (let* ((dx (list-ref x 9))
         (dy (list-ref y 9)))
    (cond
     ((eq? dx 'empty)
      #f)
     ((eq? dy 'empty)
      #t)
     (else
      (< (f dx) (f dy))))))
         
(define li2b (map (lambda (x)
                   (sort x less))
                 li2))


(define li3 (let loop ((l li2b))
              (if (pair? l)
                  (if (or (member (list-ref (caar l) 4) diagnoser)
                          (let ((x (list-ref (caar l) 5)))
                            (if (pair? x)
                                (or-map (lambda (x) (member x diagnoser))
                                        x)
                                (member x diagnoser))))
                      (cons (car l) (loop (cdr l)))
                      (loop (cdr l)))
                 '())))

(pk 'length3 (length li3))
(pk 'part3-done)

;id kön alder lan HDIA diagnos Sjukhus lt_lin mvo indatum utdatum ar vtid 
;insatt utsatt pvård ekod1 ... ekod 9 op pekare opd1 .... opd30

(define li4 (let loop ((l li3))
              (match l
                ((((id kon alder . _) . _) . u)
                 (if (< alder 15)
                     (loop u)
                     (cons (car l) (loop u))))
                (() '()))))

(pk 'length4 (length li4)) 
(define (pick-id id) 
  (lambda (x) 
    (if (and (= id 64) (eq? (car x) 64)) 
        (pk id x))
    (if (eq? id (car x)) x #f)))

(define amps '())
(define nocodes '())
(define plasts  '())
(define mocodes '())
(define (f id x)
  (if (= x -2)
      (begin
	(set! plasts (cons id plasts))
	x)
      x))
(define (f2 id x)
  (if (= x 6)
      (begin
	(set! mocodes (cons id mocodes))
	x)
      x))

(define (los l)
  (define (time-it x)
    (+ (* 365 (date-year x))
       (* 30  (date-month x))
       (date-day x)))

  (let lp ((l l) (s 0))
    (if (pair? l)
	(let ((x (car l)))
	  (catch #t
	    (lambda ()
	      (+ (- (time-it (list-ref x 10)) 
		    (time-it (list-ref x 9)))
		 s))
	    (lambda x 0)))
	s)))
(define basic (let loop ((l li4) (r '()))
                (match l
                  (( (and me ((and first (id kon alder lan . _)) . _)) . u)
                   (if (eq? (first-date me) 'empty) (begin id (loop u r))
                   (let ((död   #f)
                         (code  'nocode)
			 (doplasts #t)
                         (år  (list-ref first 11)))                    
                     (loop u
                           (cons `(,id ,år ,kon ,alder
			       ,(f id (if (or-map plastic-no-a00? me)
				    (let lp ((l me))
				      (if (pair? l)
					  (aif (d) (nplastic? (car l))
					       (aif (d) (time-to-base2 
							 (aif (d) 
							      (opdate-ref 
							       (car l) d)
							      d
							      -1)
							 first)
						    (begin
						      (if (> d 1000)
							  (pk 'oups id))
						      d)
						    -2)
					       (lp (cdr l)))
					  -3))
				    -4))
			       ,(let ((dod (aif (dd) (or-map (pick-id id) dor)
                                                (begin
                                                  (set! död #t)
                                                  (set! code (caddr dd))
                                                  (time-to-base (cadr dd)
                                                                first))
                                                -1)))
                                  (if död dod (time-to-end first))
                                  #;(if (> dod 0)
                                      dod
                                      100000)
                                  #;(aif (it) (or-map amputition? me)
                                        (begin
                                          (set! död #f)
                                          (- (time-to-end first) it))
                                        (if död dod (time-to-end first))))
                               ,(if död 1 0)
                               ,code
			       ,(if (amputition? first) 
                                   1
                                   0)
			       ,(if (or-map  amputition? me) 
                                    (begin
                                      (set! amps (cons id amps))
                                      1)
                                    0)
			       ,(amp-3 me 90)
                               ,(amp-3 me (* 2 365))
			       ,(if (or-map plastic?     me) 1 0)
			       ,(if (or-map plastic-int? me) 1 0)
                               ,(if (or-map plastic-a?   me) 1 0)
                               ,(if (or-map plastic-r?   me) 1 0)
                               ,(if (or-map plastic-q?   me) 1 0)
                               ,(let loop ((me me) (r '()))
                                  (if (pair? me)
                                      (loop (cdr me) (append (get-opd (car me)) r))
                                      r))
			       ,(make-opdata me)
			       ,(make-plastic-opdata me)
			       ,(digitize (find-diagnose 
					   (list-ref diagnoser 0) first))
			       ,(digitize (find-diagnose 
					   (list-ref diagnoser 1) first))
			       ,(digitize (find-diagnose 
					   (list-ref diagnoser 2) first))
                               ,(get-accident-type   me)
                               ,(get-accident-type-2 me)
                               ,(month-first me)
			       ,@(let ((a (if (or-map ex?   me) 1 0))
				       (b (if (or-map ms?   me) 1 0))
				       (c (if (or-map pl?   me) 1 0))
				       (d (if (or-map ngj?  me) 1 0)))
				   (when (not (or-map (lambda (x) (= x 1)) (list a b c d)))
					 (set! nocodes (cons id nocodes)))
				   (list a b c d))
			       ,(if (or-map arest?   me) 1 0)
			       ,(if (or-map ex-ms?   me) 1 0)
			       ,(if (or-map ex-pl?   me) 1 0)
			       ,(if (or-map ex-re?   me) 1 0)
			       ,(los me)
			       ,(f2 id (the-op-code me)))
				 r)))))
                  (() (reverse r)))))


(with-output-to-file "amputations.csv"
  (lambda ()
    (let lp ((l (sort amps <)))
      (if (pair? l)
          (let lp2 ((ll (hash-ref pat-map (car l) '())))
            (if (pair? ll)
                (begin
                  (format #t "~a~%" (caar ll))
                  (lp2 (cdr ll)))
                (begin
                  (format #t "~%")
                  (lp (cdr l)))))))))

(with-output-to-file "plasts.csv"
  (lambda ()
    (let lp ((l (sort plasts <)))
      (if (pair? l)
          (let lp2 ((ll (hash-ref pat-map (car l) '())))
            (if (pair? ll)
                (begin
                  (format #t "~a~%" (caar ll))
                  (lp2 (cdr ll)))
                (begin
                  (format #t "~%")
                  (lp (cdr l)))))))))

(with-output-to-file "nocode.csv"
  (lambda ()
    (let lp ((l (sort nocodes <)))
      (if (pair? l)
	  (if #t
	      (let lp2 ((ll (hash-ref pat-map (car l) '())))
		(if (pair? ll)
		    (begin
		      (format #t "~a~%" (caar ll))
		      (lp2 (cdr ll)))
		    (begin
		      (format #t "~%")
		      (lp (cdr l)))))
	      (lp (cdr l)))))))

(with-output-to-file "mocode.csv"
  (lambda ()
    (let lp ((l (sort mocodes <)))
      (if (pair? l)
	  (if #t
	      (let lp2 ((ll (hash-ref pat-map (car l) '())))
		(if (pair? ll)
		    (begin
		      (format #t "~a~%" (caar ll))
		      (lp2 (cdr ll)))
		    (begin
		      (format #t "~%")
		      (lp (cdr l)))))
	      (lp (cdr l)))))))

(pk 'part-4 (length basic))

(define (count-diagnos id)
  (let loop ((l li4) (n 0))
    (match l
      ((x . l)
       (if (or-map (lambda (x)
                     (match x
                       ((_ _ _ _ hdia diagnos . _)
                        (or (if (pair? diagnos)
                                (member id diagnos)
                                (eq? id 'diagnos))
                            (eq? id hdia)))
                       (_ #f)))
                   x)
           (loop l (+ n 1))
           (loop l n)))
      (()
       n))))

(define n-S8211 (count-diagnos 'S8211))
(define n-S8221 (count-diagnos 'S8221))
(define n-S8231 (count-diagnos 'S8231))
(define n-S9201 (count-diagnos 'S9201))
(define n-S9211 (count-diagnos 'S9211))
(define n-S9221 (count-diagnos 'S9221))

(+ n-S8211 n-S8221 n-S8231 n-S9201 n-S9211 n-S9221)
                   

(define (find-kon i)
  (let loop ((l basic) (n 0))
    (match l
      (((kon alder) . l)
       (if (eq? kon i)
           (loop l (+ n 1))
           (loop l n)))
      (() n))))

(define (only-a00 x)
  (let* ((r    (reverse x))
        (a00  (car r))
        (rest (or-map (lambda (x) (= x 1)) (cdr r))))
    (if (and (= a00 1) (not rest))
        1 
        0)))

(let ((stream-basic (open-file "basic-all.csv" "w")))    
  (format stream-basic "id,år,kon,alder,tplastic,dt,dod,dodcode,amp-1,amp,amp-3,amp-20~{,~a~},plastic,only_a00,pl_int,pl_a,pl_r,pl_q~{,~a~},S8211,S8221,S8231,diagnos,codes,accident,accident2,first-month,exfix,mspik,platt,ngjrest,meta,exfix-mspik,exfix-platt,exfix-re,the-op,los~%" 
          ampcodes plastic-codes)
  (let loop ((l basic))
    (define (mkkön x) (if (= x 1) 'male 'woman))
    (define (mk-diagnos x y z)
      (cond  
       ((= x 1) 'S8211)
       ((= y 1) 'S8221)
       ((= z 1) 'S8231)
       (#t      'NONE)))
    (define (mk-true x) (if (= x 1) 'true 'false))
    (define (x- amp3 amp20)
      (if (= amp3 1) 0 (if (= amp20 1) 1 0)))
    (match l
     (((id ar kon alder tplast dt dod d-code amp1 amp amp3 amp20 plast pint pa pr pq codes opkind plastic d1 d2 d3 acc acc2 fm a b c d q e f g los the-op) 
       . l)      
      (if (and #f (not (= plast 1))) (loop l) (begin
      (if dt (if (< dt 0) (pk `(id ,id dt ,dt dod ,dod amp ,amp)))
          (pk `(id ,id dt ,dt dod ,dod amp ,amp)))
      (when (and (number? dt) (>= dt 0) (or 1 (and dt (> dod 0))))
      (format stream-basic 
	      "~a,~a,~a,~a,~a,~a,~a,~a,~a,~a,~a,~a~{,~a~},~a,~a,~a,~a,~a,~a~{,~a~},~a,~a,~a,~a,'~a',~a,~a,~a,~a,~a,~a,~a,~a,~a,~a,~a,~a,~a~%" 
	      id (- ar 2000) (mkkön kon) (- alder 15) tplast dt dod d-code amp1 amp amp3 (x- amp3 amp20) opkind (mk-true plast) (only-a00 plastic) (mk-true pint) (mk-true pa) (mk-true pr) (mk-true pq)
	      plastic d1 d2 d3 (mk-diagnos d1 d2 d3) codes acc acc2 fm a b c d q e f g the-op los))
      (loop l))))
     (()
      (close stream-basic) 
      '()))))

(pk 'part-5)

(define-syntax incr
  (syntax-rules ()
    ((incr x  ) (set! x (+ x 1)))
    ((incr x w) (set! x (+ x w)))))

(define kön-statistik-man-kvinna
  (let ((x         0) (y            0)
	(x0        0) (y0           0)
	(x1        0) (y1           0)
	(x2        0) (y2           0)
	(x3        0) (y3           0)
	(x4        0) (y4           0)
	(n2        0) (m2           0)
	(n3        0) (m3           0)
	(n4        0) (m4           0)
	(man-tot   0) (kvinna-tot   0)
	(nman      0) (nkvinna      0)
	(nman-amp1 0) (nkvinna-amp1 0)
	(nman-amp  0) (nkvinna-amp  0)
	(nman-amp3 0) (nkvinna-amp3 0)
	(iman      0) (ikvinna      0)
	(iman-amp1 0) (ikvinna-amp1 0)
	(iman-amp  0) (ikvinna-amp  0)
	(iman-amp3 0) (ikvinna-amp3 0)
	(man       0) (kvinna       0)
	(man-amp1  0) (kvinna-amp1  0)
	(man-amp   0) (kvinna-amp   0)
	(man-amp3  0) (kvinna-amp3  0))
    (define (w   k age) (if (= k 1) (incr x  age) (incr y  age)))
    (define (w0  k age) (if (= k 1) (incr x0 age) (incr y0 age)))
    (define (w1  k age) (if (= k 1) (incr x1 age) (incr y1 age)))
    (define (w2  k age) (if (= k 1) (incr x2 age) (incr y2 age)))
    (define (w3  k age) (if (= k 1) (incr x3 age) (incr y3 age)))
    (define (w4  k age) (if (= k 1) (incr x4 age) (incr y4 age)))
    (define (q2  k) (if (= k 1) (incr n2) (incr m2)))
    (define (q3  k) (if (= k 1) (incr n3) (incr m3)))
    (define (q4  k) (if (= k 1) (incr n4) (incr m4)))
    (define (kk k) (if (= k 1) (incr man-tot)   (incr kvinna-tot)))
    (define (k1 k) (if (= k 1) (incr nman)      (incr nkvinna)))
    (define (k2 k) (if (= k 1) (incr nman-amp1) (incr nkvinna-amp1)))
    (define (k3 k) (if (= k 1) (incr nman-amp ) (incr nkvinna-amp )))
    (define (k4 k) (if (= k 1) (incr nman-amp3) (incr nkvinna-amp3)))
    (define (i1 k) (if (= k 1) (incr iman)      (incr ikvinna)))
    (define (i2 k) (if (= k 1) (incr iman-amp1) (incr ikvinna-amp1)))
    (define (i3 k) (if (= k 1) (incr iman-amp ) (incr ikvinna-amp )))
    (define (i4 k) (if (= k 1) (incr iman-amp3) (incr ikvinna-amp3)))
    (define (r1 k) (if (= k 1) (incr man)       (incr kvinna)))
    (define (r2 k) (if (= k 1) (incr man-amp1)  (incr kvinna-amp1)))
    (define (r3 k) (if (= k 1) (incr man-amp )  (incr kvinna-amp )))
    (define (r4 k) (if (= k 1) (incr man-amp3)  (incr kvinna-amp3)))
    (for-each 
     (lambda (x)
       (match x
	 ((id ar kon alder dt dod amp1 amp amp3 amp20 plast pint pa pr pq codes opkind plastic d1 d2 d3 acc fm) 	 
	  (w kon alder)
	  (kk kon)
	  (when (= amp1 1) (w2 kon alder) (q2 kon))
	  (when (= amp  1) (w3 kon alder) (q3 kon))
	  (when (= amp3 1) (w4 kon alder) (q4 kon))
	  (if (= plast 1)
	      (begin
		(k1 kon) 
		(w0 kon alder)
		(if (= amp1 1) (k2 kon))
		(if (= amp  1) (k3 kon))
		(if (= amp3 1) (k4 kon)))
	      (begin
		(r1 kon) 
		(if (= amp1 1) (r2 kon))
		(if (= amp  1) (r3 kon))
		(if (= amp3 1) (r4 kon))))
	  (if (= pint 1)
	      (begin
		(w1 kon alder)
		(i1 kon) 
		(if (= amp1 1) (i2 kon))
		(if (= amp  1) (i3 kon))
		(if (= amp3 1) (i4 kon)))))))

     basic)

    `((plastic tot  ,nman      ,nkvinna)
      (plastic amp1 ,nman-amp1 ,nkvinna-amp1)
      (plastic amp  ,nman-amp  ,nkvinna-amp )
      (plastic amp3 ,nman-amp3 ,nkvinna-amp3)
      (plastic int tot  ,iman      ,ikvinna)
      (plastic int amp1 ,iman-amp1 ,ikvinna-amp1)
      (plastic int amp  ,iman-amp  ,ikvinna-amp )
      (plastic int amp3 ,iman-amp3 ,ikvinna-amp3)
      (ej plastic tot  ,man      ,kvinna)
      (ej plastic amp1 ,man-amp1 ,kvinna-amp1)
      (ej plastic amp  ,man-amp  ,kvinna-amp )
      (ej plastic amp3 ,man-amp3 ,kvinna-amp3)
      (n amp1 ,n2 ,m2)
      (n amp  ,n3 ,m3)
      (n amp3 ,n4 ,m4)
      (mean age amp1
	    ,(/ (+ 0.0 x2) n2)
	    ,(/ (+ 0.0 y2) m2))
      (mean age amp
	    ,(/ (+ 0.0 x3) n3)
	    ,(/ (+ 0.0 y3) m3))
      (mean age amp3
	    ,(/ (+ 0.0 x4) n4)
	    ,(/ (+ 0.0 y4) m4))
      (mean age
	    ,(/ (+ 0.0 x) man-tot)
	    ,(/ (+ 0.0 y) kvinna-tot))
      (mean age plastic 
	    ,(/ (+ 0.0 x0) nman)
	    ,(/ (+ 0.0 y0) nkvinna))
      (mean age plastic int
	    ,(/ (+ 0.0 x1) iman)
	    ,(/ (+ 0.0 y1) ikvinna)))))

;Amputation vid första index vårdtillfället
